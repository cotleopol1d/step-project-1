"use strict"
const ourWorksFilterValues = {
    ourWorksItemCollection: document.getElementsByClassName("our-works-item"),
    ourWorksTabCollection: document.querySelectorAll(".our-works-tab"),
    lastActiveTab: document.querySelectorAll(".our-works-tab")[0],
    ourWorksContentList: document.querySelector(".our-works-content"),
    loadingBtn: document.querySelector(".load-wrapper.first"),
    loadingAnimation: document.querySelector(".loading-simulation-wrapper.first"),
    docFragment: document.createDocumentFragment(),
    log: document.querySelectorAll(".our-works-tab"),
}
ourWorksFilterValues.lastActiveTab.classList.add("active");

const peopleSliderValues = {
    section: document.querySelector(".people-say"),
    peoplesWrapper: document.getElementsByClassName("people-say-photos-wrapper")[0],
    peoplesNames: document.querySelectorAll(".people-say-name"),
    peoplesCategory: document.querySelectorAll(".people-say-category"),
    peoplesText: document.querySelectorAll(".people-say-text"),
    mainPhotoWrapper: document.querySelector(".people-say-photo-wrapper"),
    peoplesHidden: document.getElementsByClassName("people-say-photos-wrapper-hidden")[0],
    currentTarget: 0,
    targetName: 0,
    mainPhoto: 0,
    clickTargetStop: true,
    addValues: function () {
        let allPeoples = this.peoplesWrapper.getElementsByClassName("people-say-small-photo-wrapper");
        let arrPeoplesCharacteristics = [this.peoplesNames, this.peoplesCategory, this.peoplesText];
        let peoplesHiddenCollection = this.peoplesHidden.getElementsByClassName("people-say-small-photo-wrapper");
        peopleSliderValues.allPeoples = allPeoples;
        peopleSliderValues.arrPeoplesCharacteristics = arrPeoplesCharacteristics;
        peopleSliderValues.peoplesHiddenCollection = peoplesHiddenCollection;
    }
}
peopleSliderValues.addValues();

const galleryImagesLoadingValues = {
    galleryItemCollection: document.getElementsByClassName("gallery-item"),
    galleryBestImages: document.querySelector(".gallery-best-images"),
    galleryList: document.querySelector(".gallery-list"),
    loadingBtn: document.querySelector(".load-wrapper.second"),
    loadingAnimation: document.querySelector(".loading-simulation-wrapper.second"),
    increaseButton: document.querySelector(".gallery-increase-icon"),
    galleryImagesHiddenList: document.querySelector(".gallery-list-hidden"),
    docFragment: document.createDocumentFragment(),
    bigCurrentImg: 0,
    backgroundForBigImage: 0,
    bigImgWrapper: 0,
    bigImgCloseBtn: 0,
}
let currentTargetClick;
let currentTargetKeyDown = 0;

document.body.addEventListener("click", function (e) {
    currentTargetClick = e.target;
    if (currentTargetClick.closest("a")) {
        e.preventDefault();
    }
    if (currentTargetClick.closest(".our-works")) {
        ourWorksFilter(ourWorksFilterValues);
    }
    if (currentTargetClick.closest(".people-say-slider")) {
        peopleSlider(peopleSliderValues);
    }
    if (currentTargetClick.closest(".gallery-best-images")) {
        galleryImagesLoading(galleryImagesLoadingValues);
    }
    if (currentTargetClick.closest(".big-img-background")) {
        galleryImagesLoading(galleryImagesLoadingValues);
    }

});
document.body.addEventListener("keydown", function (e) {
    if (e.key === "Enter") {
        if (e.target.closest(".people-say-slide")) {
            e.preventDefault();
            currentTargetKeyDown = e.target;
            peopleSlider();
        }
    }
});

function ourServicesContentShow() {
    const ourServices = document.querySelector(".our-services");
    const ourServicesItemCollection = document.querySelectorAll(".our-services-item");
    ourServices.addEventListener("focusin", (e) => {
        for (const item of ourServicesItemCollection) {
            item.style.display = "none";
            if (item.dataset.ourServicesItem === e.target.textContent) {
                item.style.display = "flex";
            }
        }
    });
    ourServices.addEventListener("focusout", () => {
        for (const item of ourServicesItemCollection) {
            item.style.display = "none";
        }
    });
}

function ourWorksFilter(ourWorksFilterValues) {
    if (currentTargetClick.closest(".our-works-load")) {
        if (ourWorksFilterValues.ourWorksItemCollection.length < 36 && window.getComputedStyle(ourWorksFilterValues.loadingAnimation).display === "none") {
            ourWorksFilterValues.loadingAnimation.style.display = "block";
            ourWorksFilterValues.loadingBtn.style.visibility = "hidden";
            setTimeout(() => {
                ourWorksFilterValues.loadingAnimation.style.display = "none";
                ourWorksFilterValues.loadingBtn.style.visibility = "visible";
                if (ourWorksFilterValues.ourWorksItemCollection.length <= 12) {
                    ourWorksFilterValues.loadingAnimation.style.display = "none";
                    ourWorksFilterValues.loadingBtn.style.visibility = "visible";
                    const itemToCopy = document.querySelector(".our-works-item:first-of-type");
                    for (let x = 0; x < 12; x++) {
                        if (x === 0 || x === 3 || x === 7 || x === 11) {
                            const newItem = itemToCopy.cloneNode(true);
                            newItem.querySelector(".our-works-category").textContent = "Web Design";
                            if (x === 0) {
                                newItem.querySelector("img").src = "images/web%20design/web-design3.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                            if (x === 3) {
                                newItem.querySelector("img").src = "images/web%20design/web-design4.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                            if (x === 7) {
                                newItem.querySelector("img").src = "images/web%20design/web-design5.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                            if (x === 11) {
                                newItem.querySelector("img").src = "images/web%20design/web-design6.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                        }
                        if (x === 1 || x === 5 || x === 9) {
                            const newItem = itemToCopy.cloneNode(true);
                            newItem.querySelector(".our-works-category").textContent = "Wordpress";
                            if (x === 1) {
                                newItem.querySelector("img").src = "images/wordpress/wordpress4.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                            if (x === 5) {
                                newItem.querySelector("img").src = "images/wordpress/wordpress5.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                            if (x === 9) {
                                newItem.querySelector("img").src = "images/wordpress/wordpress6.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                        }
                        if (x === 2 || x === 4 || x === 8) {
                            const newItem = itemToCopy.cloneNode(true);
                            newItem.querySelector(".our-works-category").textContent = "Graphic Design";
                            if (x === 2) {
                                newItem.querySelector("img").src = "images/graphic%20design/graphic-design5.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                            if (x === 4) {
                                newItem.querySelector("img").src = "images/graphic%20design/graphic-design6.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                            if (x === 8) {
                                newItem.querySelector("img").src = "images/graphic%20design/graphic-design7.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                        }
                        if (x === 6 || x === 10) {
                            const newItem = itemToCopy.cloneNode(true);
                            newItem.querySelector(".our-works-category").textContent = "Landing Pages";
                            if (x === 6) {
                                newItem.querySelector("img").src = "images/landing%20page/landing-page3.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                            if (x === 10) {
                                newItem.querySelector("img").src = "images/landing%20page/landing-page4.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                        }
                    }
                }
                if (ourWorksFilterValues.ourWorksItemCollection.length >= 24 && ourWorksFilterValues.ourWorksItemCollection.length < 36) {
                    const itemToCopy = document.querySelector(".our-works-item:first-of-type");
                    for (let x = 0; x < 12; x++) {
                        if (x === 4) {
                            const newItem = itemToCopy.cloneNode(true);
                            newItem.querySelector(".our-works-category").textContent = "Web Design";
                            newItem.querySelector("img").src = "images/web%20design/web-design7.jpg";
                            ourWorksFilterValues.docFragment.append(newItem);
                        }
                        if (x === 0 || x === 3 || x === 8 || x === 11) {
                            const newItem = itemToCopy.cloneNode(true);
                            newItem.querySelector(".our-works-category").textContent = "Wordpress";
                            if (x === 0) {
                                newItem.querySelector("img").src = "images/wordpress/wordpress7.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                            if (x === 3) {
                                newItem.querySelector("img").src = "images/wordpress/wordpress8.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                            if (x === 8) {
                                newItem.querySelector("img").src = "images/wordpress/wordpress10.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                            if (x === 11) {
                                newItem.querySelector("img").src = "images/wordpress/wordpress9.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                        }
                        if (x === 2 || x === 5 || x === 7 || x === 9) {
                            const newItem = itemToCopy.cloneNode(true);
                            newItem.querySelector(".our-works-category").textContent = "Graphic Design";
                            if (x === 2) {
                                newItem.querySelector("img").src = "images/graphic%20design/graphic-design8.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                            if (x === 5) {
                                newItem.querySelector("img").src = "images/graphic%20design/graphic-design9.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                            if (x === 7) {
                                newItem.querySelector("img").src = "images/graphic%20design/graphic-design10.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                            if (x === 9) {
                                newItem.querySelector("img").src = "images/graphic%20design/graphic-design11.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                        }
                        if (x === 1 || x === 6 || x === 10) {
                            const newItem = itemToCopy.cloneNode(true);
                            newItem.querySelector(".our-works-category").textContent = "Landing Pages";
                            if (x === 1) {
                                newItem.querySelector("img").src = "images/landing%20page/landing-page5.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                            if (x === 6) {
                                newItem.querySelector("img").src = "images/landing%20page/landing-page6.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                            if (x === 10) {
                                newItem.querySelector("img").src = "images/landing%20page/landing-page7.jpg";
                                ourWorksFilterValues.docFragment.append(newItem);
                            }
                        }
                    }
                    ourWorksFilterValues.loadingBtn.style.visibility = "hidden";
                }
                for (const content of ourWorksFilterValues.docFragment.childNodes) {
                    const categoryName = content.querySelector(".our-works-category").textContent;
                    content.classList.remove("hidden");
                    if (categoryName !== ourWorksFilterValues.lastActiveTab.textContent) {
                        content.classList.add("hidden");
                    }
                    if (ourWorksFilterValues.lastActiveTab.textContent === "All") {
                        content.classList.remove("hidden");
                    }
                }
                ourWorksFilterValues.ourWorksContentList.append(ourWorksFilterValues.docFragment);
            }, 2000);
        }
    }
    if (currentTargetClick.closest(".our-works-tab")) {
        for (const tab of ourWorksFilterValues.ourWorksTabCollection) {
            tab.classList.remove("active");
        }
        currentTargetClick.classList.add("active");
        ourWorksFilterValues.lastActiveTab = currentTargetClick;
        for (const content of ourWorksFilterValues.ourWorksItemCollection) {
            const categoryName = content.querySelector(".our-works-category").textContent;
            content.classList.remove("hidden");
            if (categoryName !== currentTargetClick.textContent) {
                content.classList.add("hidden");
            }
            if (currentTargetClick.textContent === "All") {
                content.classList.remove("hidden");
            }
        }
    }
}

function peopleSlider() {
    function clickTarget() {
        peopleSliderValues.clickTargetStop = false;
        if (peopleSliderValues.currentTarget.closest(".people-say-slide")) {
            for (const activeTarget of peopleSliderValues.allPeoples) {
                if (activeTarget.className.includes("active")) {
                    activeTarget.classList.remove("active");
                    if (activeTarget === peopleSliderValues.allPeoples[3] && !peopleSliderValues.currentTarget.className.includes("left-slide") || activeTarget === peopleSliderValues.allPeoples[0] && !peopleSliderValues.currentTarget.className.includes("right-slide")) {
                        let elementToShadow;
                        let btnDirection = peopleSliderValues.currentTarget.className.includes("right-slide");
                        if (btnDirection) {
                            elementToShadow = peopleSliderValues.allPeoples[0];
                            if (!peopleSliderValues.peoplesHiddenCollection[0]) {
                                peopleSliderValues.peoplesHidden.append(elementToShadow[0]);
                            }
                        } else {
                            elementToShadow = peopleSliderValues.allPeoples[3];
                            if (!peopleSliderValues.peoplesHiddenCollection[0]) {
                                peopleSliderValues.peoplesHidden.append(elementToShadow[3]);
                            }
                        }
                        const hiddenElement = elementToShadow.cloneNode(true);
                        const swipeBan = document.createElement("div");
                        peopleSliderValues.section.prepend(swipeBan);
                        swipeBan.style.cssText = "position:absolute; width: 100%; height:100%; overflow:hidden; z-index:2;bottom:0";
                        peopleSliderValues.peoplesHidden.append(hiddenElement);
                        if (btnDirection) {
                            peopleSliderValues.currentTarget = peopleSliderValues.peoplesHiddenCollection[0];
                            peopleSliderValues.currentTarget.classList.add("new-element-right");
                        } else {
                            peopleSliderValues.currentTarget = peopleSliderValues.peoplesHiddenCollection[peopleSliderValues.peoplesHiddenCollection.length - 1];
                            peopleSliderValues.currentTarget.classList.add("new-element-left");
                        }
                        setTimeout(() => {
                            peopleSliderValues.peoplesWrapper.style.overflow = "hidden";
                            if (btnDirection) {
                                peopleSliderValues.peoplesWrapper.append(peopleSliderValues.currentTarget);
                            } else {
                                peopleSliderValues.peoplesWrapper.prepend(peopleSliderValues.currentTarget);
                            }
                            for (const targets of peopleSliderValues.allPeoples) {
                                setTimeout(() => {
                                    if (btnDirection) {
                                        targets.classList.add("animation-right");
                                        peopleSliderValues.currentTarget.classList.add("animation-right");
                                    } else {
                                        targets.classList.add("animation-left");
                                        peopleSliderValues.currentTarget.classList.add("animation-left");
                                    }
                                }, 10);
                                setTimeout(() => {
                                    if (btnDirection) {
                                        targets.classList.remove("animation-right");
                                    } else {
                                        targets.classList.remove("animation-left");
                                    }
                                }, 500);
                            }
                            setTimeout(() => {
                                elementToShadow.remove();
                                peopleSliderValues.peoplesWrapper.style.overflow = "visible";
                                if (btnDirection) {
                                    peopleSliderValues.currentTarget.classList.remove("new-element-right");
                                    peopleSliderValues.currentTarget.classList.remove("animation-right");
                                } else {
                                    peopleSliderValues.currentTarget.classList.remove("new-element-left");
                                    peopleSliderValues.currentTarget.classList.remove("animation-left");
                                }
                                setTimeout(() => {
                                    peopleSliderValues.currentTarget.classList.add("active");
                                    peopleSliderValues.mainPhoto = peopleSliderValues.currentTarget.children[0];
                                    peopleSliderValues.targetName = peopleSliderValues.mainPhoto.dataset.name;
                                    swipeBan.remove();
                                    photoCategoryTextChanger();
                                }, 10);
                            }, 500);
                        }, 50);
                        return;
                    } else {
                        if (peopleSliderValues.currentTarget.closest(".people-say-left-slide")) {
                            peopleSliderValues.currentTarget = activeTarget.previousElementSibling;
                        } else {
                            peopleSliderValues.currentTarget = activeTarget.nextElementSibling;
                        }
                        peopleSliderValues.currentTarget.classList.add("active");
                        peopleSliderValues.mainPhoto = peopleSliderValues.currentTarget.children[0];
                        peopleSliderValues.targetName = peopleSliderValues.mainPhoto.dataset.name;
                    }
                    break;
                }
            }
        } else {
            if (!peopleSliderValues.currentTarget.dataset.name) {
                peopleSliderValues.currentTarget = peopleSliderValues.currentTarget.childNodes[0];
            }
            peopleSliderValues.targetName = peopleSliderValues.currentTarget.dataset.name;
            peopleSliderValues.mainPhoto = peopleSliderValues.currentTarget;
            for (const targets of peopleSliderValues.allPeoples) {
                targets.classList.remove("active");
                peopleSliderValues.currentTarget.parentElement.classList.add("active");
            }
        }

        if (peopleSliderValues.currentTarget.closest(".people-say-small-photo-wrapper") || peopleSliderValues.currentTarget.closest(".people-say-slide")) {
            if (peopleSliderValues.currentTarget.closest(".people-say-left-slide") && peopleSliderValues.allPeoples[0].className.includes("active") || peopleSliderValues.currentTarget.closest(".people-say-right-slide") && peopleSliderValues.allPeoples[3].className.includes("active")) {
                return;
            }
            photoCategoryTextChanger();
        }

        function photoCategoryTextChanger() {
            peopleSliderValues.arrPeoplesCharacteristics.forEach((value) => {
                for (const valueElement of value) {

                    valueElement.classList.add("hidden");
                    if (valueElement.dataset.name === peopleSliderValues.targetName) {
                        valueElement.classList.remove("hidden");
                    }
                }
            });
            peopleSliderValues.mainPhotoWrapper.childNodes[0].remove();
            const newMainPhoto = peopleSliderValues.mainPhoto.cloneNode(true);
            newMainPhoto.className = "people-say-photo";
            peopleSliderValues.mainPhotoWrapper.append(newMainPhoto);
            peopleSliderValues.clickTargetStop = true;
            currentTargetKeyDown = 0;
        }
    }

    if (currentTargetKeyDown !== 0) {
        if (peopleSliderValues.clickTargetStop) {
            peopleSliderValues.currentTarget = currentTargetKeyDown;
            clickTarget();
        }
    } else {
        if (currentTargetClick.closest(".people-say-small-photo-wrapper") || currentTargetClick.closest(".people-say-slide")) {
            if (currentTargetClick.className.includes("active") || currentTargetClick.parentElement.className.includes("active")) {
                return;
            }
            peopleSliderValues.currentTarget = currentTargetClick;
            clickTarget();
        }
    }
}

function galleryImgMasonry() {
    const grid = document.querySelector('.gallery-list');
    const iso = new Isotope(grid, {
        itemSelector: '.gallery-item',
        layoutMode: 'masonry',
        masonry: {
            columnWidth: 17
        }
    });
    imagesLoaded(grid).on('progress', function () {
        // layout Isotope after each image loads
        iso.layout();
    });

}

function galleryImagesLoading() {
    if (currentTargetClick.closest(".gallery-increase-icon")) {
        let currentImage = currentTargetClick;
        while (currentImage.className !== "gallery-increase-icon") {
            currentImage = currentImage.parentElement;
        }
        currentImage = currentImage.parentElement;
        currentImage = currentImage.querySelector(".gallery-img");
        galleryImagesLoadingValues.bigCurrentImg = currentImage.cloneNode(false);
        galleryImagesLoadingValues.backgroundForBigImage = document.createElement("div");
        galleryImagesLoadingValues.bigImgWrapper = document.createElement("div");
        galleryImagesLoadingValues.bigImgCloseBtn = document.createElement("button");
        galleryImagesLoadingValues.bigCurrentImg.style.cssText = "width:100%; height:100%; max-width:100vw;max-height:100vh";
        galleryImagesLoadingValues.bigCurrentImg.classList.add(".gallery-increase-img");
        galleryImagesLoadingValues.bigImgWrapper.classList.add("big-img-wrapper");
        galleryImagesLoadingValues.bigImgWrapper.style.cssText = "width:fit-content; margin: auto; position: relative; top:50%; transform:translate(0, -50%); z-index:-1";
        galleryImagesLoadingValues.backgroundForBigImage.style.cssText = "position:fixed; top: 0; left: 0; width: 100vw; height: 100vh; z-index:10; background-color: rgba(0, 0, 0, 0.86);";
        galleryImagesLoadingValues.backgroundForBigImage.classList.add("big-img-background");
        galleryImagesLoadingValues.bigImgCloseBtn.classList.add("big-img-close");
        document.body.style.overflow = "hidden";
        document.body.prepend(galleryImagesLoadingValues.backgroundForBigImage);
        galleryImagesLoadingValues.bigImgWrapper.append(galleryImagesLoadingValues.bigImgCloseBtn);
        galleryImagesLoadingValues.bigImgWrapper.append(galleryImagesLoadingValues.bigCurrentImg);
        galleryImagesLoadingValues.backgroundForBigImage.append(galleryImagesLoadingValues.bigImgWrapper);
    }
    if (currentTargetClick.closest(".big-img-close") || currentTargetClick === galleryImagesLoadingValues.backgroundForBigImage) {
        galleryImagesLoadingValues.backgroundForBigImage.remove();
        document.body.style.overflow = "visible";
    }
    if (currentTargetClick.closest(".our-works-load")) {
        galleryImagesLoadingValues.loadingAnimation.style.display = "block";
        galleryImagesLoadingValues.loadingBtn.style.visibility = "hidden";
        setTimeout(() => {
            galleryImagesLoadingValues.loadingAnimation.style.display = "none";
            for (const hiddenImg of galleryImagesLoadingValues.galleryImagesHiddenList.children) {
                galleryImagesLoadingValues.docFragment.append(hiddenImg);
            }
            galleryImagesLoadingValues.galleryList.append(galleryImagesLoadingValues.docFragment);
            galleryImgMasonry();
        }, 2000);
    }
}


ourServicesContentShow();
galleryImgMasonry();